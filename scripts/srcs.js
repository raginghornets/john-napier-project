function jnbib(){
	document.getElementById("text").innerHTML = 
	`
	<a href='../sources/proof.html'>Proof</a>
	<p style='font-family: Times New Roman'>
		"John Napier Discovers Logarithms." 
		<i>Science and Its Times.</i> 
		Ed. Neil Schlager and Josh Lauer. Vol. 3. Detroit: Gale, 2001. 
		<i>World History in Context.</i> Web. 17 Oct. 2015.
	</p>
	<p style='font-family: Times New Roman;'>
		Scott, Joseph Frederick. "John Napier" 
		<i>Britannica.</i> 
		N.p., 2 June 2014. Web 17 Oct. 2015.
	</p>
	<p style='font-family: Times New Roman'>
		Clark, Kathleen M., and Clemency Montelle. 
		"Logarithms: The Early History of a Familiar Function - John Napier Introduces Logarithms." 
		<i>Mathematical Association of America.</i> 
		N.p., Jan. 2011. Web. 21 Oct. 2015.
	</p>
	`
}