function inventionShow() {
  document.getElementById("text").innerHTML =
    `
  <a class="define" href="https://en.wikipedia.org/wiki/Napier%27s_bones">
    - Napier"s Bones
  </a>
  <br>
  <a class="define" href="https://en.wikipedia.org/wiki/Logarithm">
    - Logarithms
  </a>
  `;
}

function earlylifeShow() {
  document.getElementById("text").innerHTML = `
  <p>The Merchiston Tower ---> 
    <img id="merchiston" src="../images/merchiston-tower-pic.png"/>
    <p id="text">   My Blender model of the Merchiston Tower ---> 
    <img id="merchistonmodel" src="../images/merchiston-tower-model.png"/>
    <ul>
      <li>Born in Merchiston Tower in Edinburgh, Scotland in 1550</li>
      <li>Died on April 4, 1617 in Edinburgh, Scotland</li>
      <li>Entered the University of St. Andrews at the age of 13 for a little bit</li>
    </ul>
  `;
}

function impactShow() {
  document.getElementById("text").innerHTML =
    `
  <ul>
    <li>Made computation and arithmetic much quicker and easier with his invention of the logarithm table</li>
  </ul>
  `;
}

function napierbonesShow() {
  document.getElementById("text").innerHTML =
    `
  <ul>
    <li>Made multiplying large numbers much faster</li>
    <li>Called "Napier's Bones" because Napier originally made it out of bones</li>
    <img src="../images/bones.png">
    <img src="../images/bones-model.png">
  </ul>
  `;
}

function logarithmShow() {
  document.getElementById("text").innerHTML = "<li>a^x=y    (a=10, find x, user input for y)</li>";
}

function funfactShow() {
  document.getElementById("text").innerHTML =
    `
  <ul>
    <li>John Napier was also signed as Neper or Nepair and he was nicknamed Marvellous Merchiston</li>
    <li>The neper, an alternative unit to the decibel, and Edinburgh Napier University in Scotland were both named after John Napier
  </ul>
  `;
}